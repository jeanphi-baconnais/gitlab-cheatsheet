# GitLab-Cheatsheet ![https://gitlab.com/jeanphi-baconnais/gitlab-cheatsheet/-/badges/release.svg](https://gitlab.com/jeanphi-baconnais/gitlab-cheatsheet/-/badges/release.svg)


This projet allows to save differents GitLab Cheatsheet. 

🔎 You can find all the cheatcheat ideas or suggestions in the [issues pages](https://gitlab.com/jeanphi.baconnais/gitlab-cheatsheet/-/issues).

## 16/09/2024: The sixteenth, CICD Catalog

![Cheatsheet 16 - CICD Catalog](ressources/GitLabCheatsheet-16-CICD-Catalog.jpg)


## 29/05/2024: The fiveteenth, GitLab Duo

![Cheatsheet 15 - GitLab Duo](ressources/GitLabCheatsheet-15-IA-Duo.jpg);

## 12/10/2022: the fourteenth, Cloud Seed 

![Cheatsheet 14 - Cloud Seed](ressources/GitLabCheatsheet-14-Cloud-Seed.jpg)


## 24/06/2022: the thirteenth, Environment


![Cheatsheet 13 - Environment](ressources/GitLabCheatsheet-13-Environments.jpg)


## 21/01/2021: the twelfth, GitLabCI Services


![Cheatsheet 12 - Kubernetes Integration](ressources/GitLabCheatsheet-12-Kubernetes.jpg)


## 21/01/2021: the eleventh, GitLabCI Services


![Cheatsheet 11 - GitLabCI Services](ressources/GitLabCheatsheet-11-GitLabCI-Services.jpg)


## 29/11/2021: the tenth, Templates and quick actions


![Cheatsheet 10 - Templates and quick actions](ressources/GitLabCheatsheet-10-Templates_and_Quick_actions.jpg)


## 28/10/2021: the nineth, GitLab API


![Cheatsheet 9 - API](ressources/GitLabCheatsheet-9-API.jpg)


## 23/07/2021: the eighth, Cache and artifacts 


![Cheatsheet 8 - Cache & Artifacts](ressources/GitLabCheatsheet-8-GitlabCI-Cache-Artifacts.jpg)


## 13/07/2021: the seventh, Service Desk 


![Cheatsheet 7 - Service Desk](ressources/GitLabCheatsheet-7-ServiceDesk.jpg)


## 06/07/2021: the sixth, registry 


![Cheatsheet 6 - Registry](ressources/GitLabCheatsheet-6-Registry.jpg)


## 29/06/2021: the fifth, order & optimize jobs 


![Cheatsheet 5 - GitLabCI-OrderAndOptimizeJobs](ressources/GitLabCheatsheet-5-GitLabCI-OrderAndOptimizeJobs.jpg)


## 22/06/2021: the fourth, jobs call sequence 


![Cheatsheet 4 - Jobs Call Sequence](ressources/GitLabCheatsheet-4-GitLabCI-Jobs_call_sequence.jpg)


## 15/06/2021: the third, GitLabCI Rules

![Cheatsheet 3 - GitLabCI Rules](ressources/GitLabCheatsheet-3-GitLabCIRules.jpg)


## 08/06/2021: the second, les runners.

![Cheatsheet 2 - Runners](ressources/GitLabCheatsheet-2-Runners.jpg)


## 01/06/2021: the first one, is about stages and jobs basics.

![Cheatsheet 1 - basics](ressources/GitLabCheatsheet-1-Basics_of_stages_&_jobs.jpg)


*Make by Jean-Phi*
